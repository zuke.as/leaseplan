@Smoke
Feature: Search for the product

  Scenario Outline: Check product count for <product>
    When I create a call for <product>
    Then I get 200 as response code
    And I get <number> products
    Examples:
      | product | number |
      | orange  | 0      |
      | apple   | 0      |
      | pasta   | 20     |
      | cola    | 20     |

  Scenario Outline: No products with price higher than <price>
    When I create a call for <product>
    Then I get 200 as response code
    And no product price should be higher than <price>
    Examples:
      | product | price |
      | orange  | 7.82  |
      | apple   | 6     |
      | pasta   | 10.1  |
      | cola    | 10.5  |

  Scenario Outline: Check successful image URL access for <product>
    When I create a call for <product>
    Then I get 200 as response code
    And the image URL works properly
    Examples:
      | product |
      | orange  |
      | apple   |
      | pasta   |
      | cola    |