package starter;

import io.cucumber.junit.CucumberOptions;
import lombok.extern.slf4j.Slf4j;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@Slf4j
@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "src/test/resources/features",
        glue = {"starter.stepdefinitions"},
        plugin = {"pretty", "json:target/cucumber-report.json"},
        stepNotifications = true,
        monochrome = true
)
public class TestRunner {
}
