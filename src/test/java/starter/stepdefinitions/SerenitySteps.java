package starter.stepdefinitions;

import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import org.apache.http.HttpStatus;
import org.junit.Assert;

import java.util.List;

@Slf4j
public class SerenitySteps {

    private String baseURI;
    private String productPath;
    private Response response;

    @Step("Set base URI to {0}")
    public void setBaseURI(String baseURI) {
        this.baseURI = baseURI;
    }

    @Step("Set product path to {0}")
    public void setProductPath(String product) {
        this.productPath = product;
    }

    @Step("Make GET request with product: {0}")
    public void makeGETRequest() {
        response = SerenityRest.given().log().all()
                .when().get(baseURI, productPath)
                .then().log().status().log().body()
                .extract().response();
    }

    @Step("Verify response status is {0}")
    public void verifyResponseStatus(int statusCode) {
        log.info("Expected status code: {}", statusCode);
        Assert.assertEquals("Incorrect response status code", statusCode, response.statusCode());
    }

    @Step("Verify number of products is {0}")
    public void verifyNumberOfProducts(int expectedNumber) {
        log.info("Expected number of products: {}", expectedNumber);
        int actualNumber = response.jsonPath().getList("$").size();
        Assert.assertEquals("The expected number of products is greater than the actual one",
                expectedNumber, actualNumber);
    }

    @Step("Verify the prices are lower than {0}")
    public void verifyPrice(float price) {
        List<Float> prices = response.jsonPath().getList("price");
        if (prices.isEmpty()) {
            log.info("No products found in the response");
        }
        float highestPrice = prices.stream()
                .max(Float::compare)
                .orElse(0.0f);
        Assert.assertTrue(String.format("The highest price %s is greater than the expected one %s", highestPrice, price),
                price > highestPrice);
    }

    @Step("Verify the image URLs")
    public void verifyImageUrls() {
        List<String> images = response.jsonPath().getList("image");

        for (String image : images) {
            log.info("Image URL: {}", image);
            int statusCode = SerenityRest.get(image).then().extract().statusCode();

            String errorMessage = String.format("The product %s has an incorrect image URL %s", productPath, image);
            Assert.assertEquals(errorMessage, HttpStatus.SC_OK, statusCode);
        }
    }
}
