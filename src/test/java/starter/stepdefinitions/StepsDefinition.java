package starter.stepdefinitions;

import io.cucumber.java.Before;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import utils.Utils;

public class StepsDefinition {

    @Steps
    private SerenitySteps serenitySteps;

    @Before
    public void setUpBaseURI() {
        serenitySteps.setBaseURI(Utils.getProperty("URL"));
    }

    @When("I create a call for {}")
    public void callCreation(String product) {
        serenitySteps.setProductPath(product);
        serenitySteps.makeGETRequest();
    }

    @Then("I get {int} as response code")
    public void checkResponseCode(int statusCode) {
        serenitySteps.verifyResponseStatus(statusCode);

    }

    @Then("I get {int} products")
    public void checkNumberOfProducts(int number) {
        serenitySteps.verifyNumberOfProducts(number);
    }

    @Then("no product price should be higher than {float}")
    public void checkPrice(float price) {
        serenitySteps.verifyPrice(price);
    }

    @Then("the image URL works properly")
    public void checkImageUrl() {
        serenitySteps.verifyImageUrls();
    }
}
