package utils;


import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public class Utils {

  public static String getProperty(String key) {
    InputStream input = Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties");
    Properties properties = new Properties();
    try {
      properties.load(input);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return properties.getProperty(key);
  }

}
