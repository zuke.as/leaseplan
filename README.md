# Regression tests repository for leaseplan

This project is a test automation framework that covers backend testing.

## Getting Started

### Prerequisites

- Git (to clone this repository)
- Java 17
- Maven

### Tools

- Serenity
- Junit
- Maven Surefire Plugin
- Serenity Maven Plugin

## Running the tests

You can run the tests using either of the following methods:

1. **Using the `TestRunner.java`**: Execute the `TestRunner.java` class. You can also use the `-Dcucumber.filter.tags`
   option to run tests with specific tags.
2. **Using Maven Commands**: Run `mvn verify` or `mvn test -D"cucumber.filter.tags=@tag"` to run tests using Maven
   commands.

## Reports

After running the tests successfully, you can find the test reports in the `target/serenity` folder. The report includes
details about test results and scenarios covered.

Example Report:
![Serenity BDD - Test Report HTML Home](src/test/resources/images/serenity-report.png)

## Troubleshooting

Encountering issues? Here are some common solutions:

- "On IntelliJ, I can't go to the Step definitions of any step in a feature file..."

  > Solution: Install the `Cucumber for Java` plugin and deactivate the `Substeps IntelliJ Plugin`. Restart IntelliJ to
  navigate to step definitions.

- "Running a single scenario from IntelliJ..."

  > Solution: Use the `tags` annotation parameter in `TestRunner.java` or run scenarios directly by clicking the `Play`
  button in feature files.

- "The annotations in the project are not working..."

  > Solution: Install the `Lombok Plugin` and enable `Annotation Processing` in IntelliJ settings.

## Gitlab pipeline

The GitLab pipeline is configured via the `.gitlab-ci.yml` file. The pipeline generates two types of reports: the
Serenity report (`target/serenity`) and the test report (`Tests` tab).

## What's new

- Transitioned from a combined Gradle and Maven setup to a dedicated Maven-based framework.
- Modularized Serenity and Cucumber steps for enhanced code organization and reusability.
- Optimized version control by updating `.gitignore` file to exclude unnecessary files.
- Improved logging experience through updates in `logback-test.xml` and `TestRunner.java` files.
- Strengthened security and configurability by migrating sensitive data to the `config.properties` file.
- Introduced new test scenarios to expand test coverage and ensure comprehensive testing.